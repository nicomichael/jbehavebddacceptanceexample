Meta:

Narrative:
As a user
I want to perform an action
So that I can achieve a business goal

Scenario: http://stackoverflow.com/questions/10730015/how-to-run-a-story-multiple-times-with-different-parameters

Given the vegtable name ${stockName}
When I enter a valid price ${stockPrice}
Then I am able to confirm the validity stock data


Examples:
|stockName  |   stockPrice  |
|carrots     |   0.25       |
