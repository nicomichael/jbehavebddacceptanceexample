package za.co.vegtable.bdd.example.steps;

import org.jbehave.core.annotations.BeforeScenario;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.core.model.ExamplesTable;
import org.junit.Assert;
import za.co.vegtable.bdd.example.Store;
import za.co.vegtable.bdd.example.Vegtable;

import java.util.Map;

/**
 * @author Nico Michael
 */
public class ValidatePriceOfTomatoesCarrotsAndPotatoesSteps {

    private Vegtable vegtable1,vegtable2,vegtable3;
    private Store store;

    @BeforeScenario
    public void scenarioSetup() {
        vegtable1=new Vegtable();
        vegtable2=new Vegtable();
        vegtable3=new Vegtable();
        store=new Store();
        System.out.println("BeforeScenario");
    }

    @Given("the vegtable of interest is the tommato $stockDetails")
    public void givenTheVegtableOfInterestIsTheTommato(@javax.inject.Named("stockDetails") ExamplesTable data) throws Exception {
        Map<String, String> row = data.getRow(0);
        String invalidIcn = row.get("icn");
        String stockName = row.get("stockName");
        String stockPrice = row.get("stockPrice");
        System.out.println("stockName: "+stockName);
        System.out.println("stockPrice: "+stockPrice);
        vegtable1.setName(stockName);
        vegtable1.setPrice(stockPrice);
    }

    @Given("the vegtable of interest is the carrot $stockDetails")
    public void givenTheVegtableOfInterestIsTheCarrot(@javax.inject.Named("stockDetails") ExamplesTable data) throws Exception {
        Map<String, String> row = data.getRow(0);
        String stockName = row.get("stockName");
        String stockPrice = row.get("stockPrice");
        System.out.println("stockName: "+stockName);
        System.out.println("stockPrice: "+stockPrice);
        vegtable2.setName(stockName);
        vegtable2.setPrice(stockPrice);
    }

    @Given("the vegtable of interest is the potato $stockDetails")
    public void givenTheVegtableOfInterestIsThePotato(@javax.inject.Named("stockDetails") ExamplesTable data) throws Exception {
        Map<String, String> row = data.getRow(0);
        String stockName = row.get("stockName");
        String stockPrice = row.get("stockPrice");
        System.out.println("stockName: "+stockName);
        System.out.println("stockPrice: "+stockPrice);
        vegtable3.setName(stockName);
        vegtable3.setPrice(stockPrice);
    }

    @When("I enter a valid vegtable of interests price $stockDetails")
    public void whenIEnterAValidPrice(@javax.inject.Named("stockDetails") ExamplesTable data) throws Exception {
        Map<String, String> row = data.getRow(0);
        String scenario = row.get("scenario");

        switch (scenario){
            case "1":
                System.out.println(vegtable1.toString());
                break;
            case "2":
                System.out.println(vegtable2.toString());
                break;
            case "3":
                System.out.println(vegtable3.toString());
                break;
        }
    }

    @Then("I am able to confirm the stock price validity of the vegtable of interest $stockDetails")
    public void IAmAbleToConfirmTheStockDataIsCorrect(@javax.inject.Named("stockDetails") ExamplesTable data) throws Exception {
        Map<String, String> row = data.getRow(0);
        String scenario = row.get("scenario");

        switch (scenario){
            case "1":
                Assert.assertTrue(store.validate(vegtable1));
                break;
            case "2":
                Assert.assertTrue(store.validate(vegtable2));
                break;
            case "3":
                Assert.assertTrue(store.validate(vegtable3));
                break;
        }

    }

}
