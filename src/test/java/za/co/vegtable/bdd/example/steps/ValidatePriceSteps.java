package za.co.vegtable.bdd.example.steps;

import org.jbehave.core.annotations.*;
import org.jbehave.core.model.ExamplesTable;
import org.junit.Assert;
import za.co.vegtable.bdd.example.Store;
import za.co.vegtable.bdd.example.Vegtable;

import javax.inject.*;
import java.util.Map;

/**
 * @author Nico Michael
 */
public class ValidatePriceSteps {
    private Vegtable vegtable;
    private Store store;

    @BeforeScenario
    public void scenarioSetup() {
        vegtable=new Vegtable();
        store=new Store();
        System.out.println("BeforeScenario");
    }



    @Given("the vegtable name ${stockName}")
    public void givenIHaveTheVegtableName(String stockName) throws Exception{
        System.out.println("stockName: "+stockName);
        vegtable.setName(stockName);
    }

    @When("I enter a valid price ${stockPrice}")
    public void whenIEnterAValidPrice(String stockPrice) throws Exception{
        System.out.println(vegtable.toString());
        vegtable.setPrice(stockPrice);
    }

    @Then("I am able to confirm the validity stock data")
    public void IAmAbleToConfirmTheStockDataIsCorrect() throws Exception {
        Assert.assertTrue(store.validate(vegtable));
    }

}
