package za.co.vegtable.bdd.example;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Nico Michael
 */
public class Store {
    List<Vegtable> stock=new ArrayList<>();

    public Store(){
        stock.add(new Vegtable("Carrots",0.25));
        stock.add(new Vegtable("Spinnage",0.75));
        stock.add(new Vegtable("Tomatoes",0.35));
        stock.add(new Vegtable("Potatoes",0.15));
    }

    public boolean validate(Vegtable vegtable) {
        for(Vegtable foundVegtable :stock){
            if(foundVegtable.equals(vegtable)){
                return true;
            }
        }
        return false;
    }
}