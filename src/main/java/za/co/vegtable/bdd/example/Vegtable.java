package za.co.vegtable.bdd.example;

/**
 *
 * @author Nico Michael
 */
public class Vegtable {
    private String name;
    private double price;

    public Vegtable() { }

    public Vegtable(String name,double price) {
        this.price = price;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setPrice(String price) {
        this.price = Double.parseDouble(price);
    }

    public boolean equals(Vegtable vegtable){
        return this.price==vegtable.getPrice() && this.name.equalsIgnoreCase(vegtable.getName());
    }

    public String toString(){
        return name+" | "+price;
    }
}