package za.co.vegtable.bdd.example.deleteme;

import java.io.IOException;

/**
 * Created by nickm on 2016/03/22.
 */
public class example {

   void method1() throws NullPointerException{}
    void method2() throws IOException{}

    void method3(){
        method1();
    }
    void method4() throws IOException{
        method2();
    }

}
