Meta:

Narrative:
As a user
I want to perform an action
So that I can achieve a business goal

Scenario: 1 : I need to validate the price of tomatoes are correct
Given the vegtable of interest is the tommato
|stockName  |   stockPrice  | scenario  |
|tomatoes   |   0.35        |   1       |

When I enter a valid vegtable of interests price
|stockName  |   stockPrice  | scenario  |
|tomatoes   |   0.35        |   1       |

Then I am able to confirm the stock price validity of the vegtable of interest
|stockName  |   stockPrice  | scenario  |
|tomatoes   |   0.35        |   1       |

Scenario: 2 : I need to validate the price of tomatoes are correct
Given the vegtable of interest is the carrot
|stockName  |   stockPrice  | scenario  |
|carrots    |   0.25        |   2       |

When I enter a valid vegtable of interests price
|stockName  |   stockPrice  | scenario  |
|carrots    |   0.25        |   2       |

Then I am able to confirm the stock price validity of the vegtable of interest
|stockName  |   stockPrice  | scenario  |
|carrots    |   0.25        |   2       |

Scenario: 3 : I need to validate the price of potatoes are correct
Given the vegtable of interest is the potato
|stockName  |   stockPrice  | scenario  |
|potatoes   |   0.15        |   3       |

When I enter a valid vegtable of interests price
|stockName  |   stockPrice  | scenario  |
|potatoes   |   0.15        |   3       |

Then I am able to confirm the stock price validity of the vegtable of interest
|stockName  |   stockPrice  | scenario  |
|potatoes   |   0.15        |   3       |

Examples:
|stockName  |   stockPrice  | scenario  |
|tomatoes   |   0.35        |   1       |
|carrots    |   0.25        |   2       |
|potatoes   |   0.15        |   3       |
